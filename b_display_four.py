import open3d as o3d
import pandas as pd
import numpy as np
from a_filter_and_segment_single import outlier_removal_sphere, plane_segmentation

def df_to_pcd(df, x_offset=0, y_offset=0, z_squish_factor=50):
    raw_z = list()
    # since z-values are in um, not coordinates
    
    for y, row in df.iterrows():
        for x, z in enumerate(row.values):
            raw_z.append([x+x_offset,y+y_offset,z/z_squish_factor])
  
    # create pointcloud from list of 3d coordinates
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(np.array(raw_z))
    return pcd

if __name__ == "__main__":

    # 2d table with rows = "pixel" row of image from above, values = z (height, in um), columns = "pixel" column
    # resolution = (541, 512)
    df_9 = pd.read_csv("./data/2023-07-11_01_demoJob-Position/9_demoJob-Position_Z_0.txt", header=0, delimiter="\t")
    df_10 = pd.read_csv("./data/2023-07-11_01_demoJob-Position/10_demoJob-Position_Z_0.txt", header=0, delimiter="\t")
    df_16 = pd.read_csv("./data/2023-07-11_01_demoJob-Position/16_demoJob-Position_Z_0.txt", header=0, delimiter="\t")
    df_15 = pd.read_csv("./data/2023-07-11_01_demoJob-Position/15_demoJob-Position_Z_0.txt", header=0, delimiter="\t")

    # arranged as followed (view from top)
    # 9   16
    # 10  15

    # add some spacing between
    x_offset = 512 + 10
    y_offset = 541 + 10

    pcd_9 = df_to_pcd(df_9)
    pcd_10 = df_to_pcd(df_10, y_offset=y_offset)
    pcd_16 = df_to_pcd(df_16, x_offset=x_offset)
    pcd_15 = df_to_pcd(df_15, x_offset=x_offset, y_offset=y_offset)

    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(pcd_9.points)
    pcd.points.extend(pcd_10.points)
    pcd.points.extend(pcd_16.points)
    pcd.points.extend(pcd_15.points)

    # show raw data point cloud (squished)
    # o3d.visualization.draw_geometries([pcd_9, pcd_10, pcd_16, pcd_15], window_name="Raw Data (PointCloud)")
    o3d.visualization.draw_geometries([pcd], window_name="Raw Data (PointCloud)")

    # remove outliers 
    pcd_filtered = outlier_removal_sphere(pcd, depth=2, display=True)

    pcd_plane = pcd_filtered 
    for i in range(3):
        plane_model, pcd_plane = plane_segmentation(pcd_plane, display=True)