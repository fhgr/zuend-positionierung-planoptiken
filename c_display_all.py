import open3d as o3d
import pandas as pd
import numpy as np
import os

from a_filter_and_segment_single import outlier_removal_sphere, plane_segmentation
from b_display_four import df_to_pcd

def file_to_pcd(file, x_offset=0, y_offset=0):
    df = pd.read_csv(file, header=0, delimiter="\t")
    return df_to_pcd(df, x_offset=x_offset, y_offset=y_offset)

def merge_pcds(pcds):
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(pcds[0].points)

    for entry in pcds[1:]:
        pcd.points.extend(entry.points)
    
    return pcd

if __name__ == "__main__":

    folder = "./data/2023-07-11_01_demoJob-Position"
    #folder = "./data/test"

    # only "Z" Files -> height. ignore "A" Files -> amplitude
    file_names = [a for a in os.listdir(folder) if "_Z_" in a]

    pcds = list()

    x_offset = 512 + 20
    y_offset = 541 + 10

    for file_name in file_names:
        index = int(file_name.split("_")[0])

        index = index + 1 if index >= 19 else index

        additional_offset = 40 if index % 2 == 0 else 0

        up = int((index-1)/6) % 2 == 0
        up = not up
        x = int((index - 1) / 6) # column -> x
        y = abs((0 if up else 5) - ((index - 1) % 6))
        file = os.path.join(folder, file_name)
        pcd = file_to_pcd(file, x_offset=x*x_offset, y_offset=y*y_offset)
        pcds.append(pcd)
    
    pcd = merge_pcds(pcds)

    # downsample massive rawdatra
    pcd_voxel_down = pcd.voxel_down_sample(voxel_size=0.02)

    o3d.visualization.draw_geometries([pcd_voxel_down], window_name="Outlier removal radius/sphere")

    # remove outliers 
    pcd_filtered = outlier_removal_sphere(pcd_voxel_down, depth=2, display=True)

    pcd_plane = pcd_filtered 
    for i in range(3):
        plane_model, pcd_plane = plane_segmentation(pcd_plane, display=True)