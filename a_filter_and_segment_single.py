import open3d as o3d
import pandas as pd
import numpy as np

# radius outlier removal
def outlier_removal_sphere(pcd, depth=1, display=True):
    cl, ind = pcd.remove_radius_outlier(nb_points=50, radius=5)

    inlier_cloud = pcd.select_by_index(ind)
    outlier_cloud = pcd.select_by_index(ind, invert=True)

    if display:
        outlier_cloud.paint_uniform_color([1, 0, 0])
        inlier_cloud.paint_uniform_color([0.8, 0.8, 0.8])
        o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud], window_name="Outlier removal radius/sphere")

    if depth <= 1:
        return inlier_cloud
    else:
        return outlier_removal_sphere(inlier_cloud, depth - 1)

def plane_segmentation(pcl, display=True):
    plane_model, inliers = pcl.segment_plane(distance_threshold=1, ransac_n=5, num_iterations=1000)
    [a, b, c, d] = plane_model
    print(f"Plane equation: {a:.2f}x + {b:.2f}y + {c:.2f}z + {d:.2f} = 0")

    inlier_cloud = pcl.select_by_index(inliers)
    outlier_cloud = pcl.select_by_index(inliers, invert=True)

    if display:
        inlier_cloud.paint_uniform_color([1.0, 0, 0])
        o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud], window_name="Plane segmentation")

    return [a,b,c,d], outlier_cloud


if __name__ == "__main__":
    
    # 2d table with rows = "pixel" row of image from above, values = z (height, in um), columns = "pixel" column
    # resolution = (541, 512)
    df = pd.read_csv("./data/2023-07-11_01_demoJob-Position/9_demoJob-Position_Z_0.txt", header=0, delimiter="\t")

    raw_z = list()
    # since z-values are in um, not coordinates
    z_squish_factor = 50

    for y, row in df.iterrows():
        for x, z in enumerate(row.values):
            raw_z.append([x,y,z/z_squish_factor])

    # create pointcloud from list of 3d coordinates
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(np.array(raw_z))

    # show raw data point cloud (squished)
    o3d.visualization.draw_geometries([pcd], window_name="Raw Data (PointCloud)")

    # downsample massive rawdatra
    pcd_voxel_down = pcd.voxel_down_sample(voxel_size=0.02)

    # remove outliers 
    pcd_filtered = outlier_removal_sphere(pcd, depth=2, display=False)
    o3d.visualization.draw_geometries([pcd_filtered], window_name="Raw Data (PointCloud)")
    
    # extract 3 planes (from biggest (by amount of backing points) to smallest). Need to 
    pcd_plane = pcd_filtered 
    for i in range(3):
        plane_model, pcd_plane = plane_segmentation(pcd_plane, display=True)
